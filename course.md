# Lehrtexte verfassen  
Diese Handreichung informiert über die Erstellung von Lehrtexten, die von Studierenden zum Wissenserwerb im Selbststudium genutzt werden sollen. 

![Advance Organizer für Lehrtexte](Advance_Organizer_Lehrtexte.png)

## Funktion: den selbständigen Wissenserwerb unterstützen
Lehrtexte enthalten aufbereitete Lehrinhalte, die von den Lernenden selbständig aufgenommen und verarbeitet werden sollen. Sie sind didaktisch konzipiert, d.h. sie unterstützen den Prozess des Wissenserwerbs durch inhaltliche und typografische Gestaltungsmerkmale. Sie sollten entsprechend 

*  selbsterklärend,
*  sequentiell, 
*  fachlich korrekt, 
*  sachlich und prägnant formuliert,
*  kohärent,
*  gendergerecht und
*  möglichst barrierefrei gestaltet sein. 

In der Praxis sind Lehrtexte aus Lehrbüchern oder als sogenannte Lehrbriefe/Skripte bekannt. 

## Inhaltliche Gestaltungselemente
Gute Lehrtexte zeichnen sich durch eine kohärente inhaltliche Textstruktur sowie durch kleinere inhaltliche Gestaltungselemente aus.
Die **inhaltliche Textstruktur** richtet sich insgesamt nach dem Gestaltungsprinzip *"Vom Allgemeinen zum Besonderen"*. Der inhaltliche Einstieg sollte einen **Überblick** über die zu thematisierenden Lehrinhalte und die intendierten Lernziele bieten. Der Überblick kann über eine Abbildung erfolgen. Allerdings können auch andere Darstellungsformen wie etwa Aufzählungen oder Beschreibungen des Vorgehens Schwerpunkte der Lehrinhalte verdeutlichen.

In der Textstruktur werden **sequenzielle Wissenseinheiten** abgebildet. Diese können je nach Umfang der Lehrinhalte in mehreren *Themen* und dazugehörigen *Themanaspekten* arrangiert werden. Dabei sind folgende Gesichtspunkte zu beachten:
 
 * Der Informationsfluss muss nachvollziehbar sein.
 * Die Information beginnt zumeist mit der Darstellung allgemeiner Aspekte, bevor sukzessive thematische Eingrenzungen vorgenommen und spezifischere Informationen behandelt werden. (Hineinzoomen), 
 * Die Inhalte sind logisch aufeinander aufgebaut und finden sich in einer klaren Gliederung wieder. Diese wird durch aussagekräftige Überschriften und Zwischenunterschriften prägnant gestaltet.
 * Die fachlich aufbereiteten Informationen werden dabei in einem deskriptiven bis explikativen Darstellungsmodus dargelegt.

Der Lehrtext wird mit einer **Zusammenfassung** der zentralen Aussagen geschlossen. Dies kann z.B. mit Hilfe eines Merkkastens oder durch eine Aufzählung (z. B. unter dem Schlagwort Lessons learned) umgesetzt werden. So können Lernende den tatsächlichen Lernerfolg abgleichen.

Mit **kleinere inhaltliche Gestaltungsmerkmale** sind wiederkehrende Muster gemeint, die bei den Beschreibungen und Ausführungen der Sachverhalte zum Einsatz kommen. So sind verwendete *Fachbegriffe* einzuführen und zu klären. Je nach Fachkultur oder Gegenstand des Lehrtextes geschieht dies durch die Übernahme von Definitionen oder durch Begriffsannäherungen. 
    
Explikationen von Sachverhalten können durch *Beispiele, Analogien und Metaphern* veranschaulicht werden. Je nach Funktion und Einsatz sollen Veranschaulichungen die Lernenden dabei unterstützen Sachverhalte besser zu erfassen. Dabei sind Veranschaulichungsmittel nicht ausschließlich textbasiert, sondern können ebenso Bilder, Grafiken und weitere Elemente sein. Wesentlich ist dabei, dass die Veranschaulichungen einen direkten Bezug zu den Inhalten aufweisen.  
    
## Typografische Gestaltungselemente
Der Lehrtext soll für Studierende sichtbar strukturiert sein. Dies kann durch eine angemessene *Schriftart* und *-größe*, angemessene *Zeilenlänge* 
und * -abstand* sowie mithilfe von *Absätzen* organisiert werden. Diese typografischen Hilfestellungen markieren, wann etwa eine Sinneinheit beginnt bzw. abgeschlossen ist. 

Ein weiteres Gestaltungselement sind *Marginalien*. In der sogenannten Marginalspalte neben dem Fließtext werden prägnante Stichworte zum Inhalt des jeweiligen Absatzes positioniert. Neben der Formulierung von Kernaussagen können auch *Symbole* eingesetzt werden, die entweder selbsterklärend oder zuvor in einer Legende erläutert worden sind. Marginalspalten sind insbesondere aus dem klassischen Lehrbuch bekannt und haben folgende Wirkungen:
    
 * vermitteln den Lernenden einen groben Überblick über den Inhalt vor dem Lesen,
 * richten den Fokus auf das Zentrale im Absatz während des Lesens,
 * dienen als Merkhilfe und und Erinnerungsstützen nach dem Lesen bzw. beim 
   wiederholten Lesen (vgl. Balzert 2016:49).

*Hervorhebungen im Text* sind ein weiteres gestalterisches Element. Sie eignen sich dazu, Aussagen, Begriffe o. ä. in den Vordergrund zu stellen und lassen sich durch unterschiedliche typografische Kniffe erzeugen: Kursiv, fett, gesperrt, unterstrichen oder eingefärbt. Hervorhebungen sollten dezent eingesetzt werden. So können Fachbegriffe z. B. zunächst kursiv hervorgehoben werden, um auf ihre Einführung im Text hinzuweisen.
    
Auch *Aufzählungen* wirken komplexitätsreduzierend. Es gibt ungeordnete, geordnete, hierarchische oder bedeutungsbezogene Aufzählungsformen. Diese werden durch Interpunktionszeichen oder Symbole gestaltet. Soll eine Reihenfolge oder eine chronologische Abfolge präsentiert werden, so eignet sich etwa eine numerische Aufzählung. Aufzählungen sollten sich z. B. durch Einrückung optisch vom Fließtext absetzen. 

In Merkboxen oder Infokästen werden die zentralen Aussagen aus dem vorangegangen Text nochmals zusammengefasst oder die präsentierten Fakten aus einer anderen Perspektive dargestellt.

## Lessons Learned
 Nach dieser Lektüre kennen Sie die Funktion von Lehrtexten und verschiedene inhaltliche und typographische Gestaltungsmerkmale. Sie sind in der Lage diese inhaltlichen Gestaltungselemente einzusetzen:
  
* [ ]  der einleitende Überblicks,
* [ ]  die Darstellung von Lernzielen,
* [ ]  die Nachvollziehbarkeit der Lerninhalte,
* [ ]  Veranschaulichungen,
* [ ]  Visualisierungen und
* [ ]  eine Ergebnissicherung am Ende des Textes.

Zusätzlich kennen Sie typografische Gestaltungselemente:

* [ ]  Wahl der Schriftart und Setzung von Absätzen zur Verbesserung der Lesbarkeit,
* [ ]  den Rückgriff auf Marginalien,
* [ ]  Hervorhebungen im Text,
* [ ]  Hervorhebungen durch Aufzählungen und
* [ ]  den Einsatz von Merkkästen.
 





    
    
    
